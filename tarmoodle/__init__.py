"""
Utilidad para crear actividades con Moodle



(c)2020 Leandro Batlle, ADICRA - GPLv3
"""
__version__ = '0.1.0'




import argparse, csv, re, time, datetime,pathlib,os,json,logging,sys
from collections import *
import pandas as pd
import jinja2 as j2
from pprint import pprint

args = None  #poblar con cargarArgumentos()
config = None #poblar con cargarconfig()
logger=logging.getLogger("tarmoodle")



class ActividadConPasos():
    """
    ActividadConPasos:

    los metodos q no comienzen con _ será pasos para la creacion de la actividad. Y son validos para comandos como:
    prepararActividad -c config.json -p paso1
    prepararActividad -c config.json -p paso2
    prepararActividad -c config.json -p paso3

    """
    global config

    def __init__(self):
#        if config is None:
#            raise ValueError("if config is None")
        self.CSVcalificaciones = None

    def _cargarCSV(self):
        csvRegexEncontrados=list()
        #
        # Buscamos Activdad.CSV descargado de Moodle
        #
        Actividad_calificacionesCSV = False
        try:
            Actividad_calificacionesCSV_recomp = config["Actividad"]["CalificacionesCSV"]. \
                replace(".","\.").format(
                Curso="(?P<Curso>\w+)",
                Actividad_Nombre="(?P<Actividad_Nombre>\w+)",
                MoodleID="(?P<MoodleID>\d+)",
                Extension="(?P<Extension>\.*csv$)")
            #Curso == config["Curso"],
            #Actividad_Nombre == config["Actividad"]["Nombre"],
            Actividad_calificacionesCSV_recomp=re.compile(Actividad_calificacionesCSV_recomp,flags=re.IGNORECASE)
            re_comp_match = None
            for _ in os.listdir(config["DirectorioEntrada"]):
                re_comp_match=Actividad_calificacionesCSV_recomp.match(_)
                if re_comp_match is not None:
                    #encontramos el archivo
                    if len(re_comp_match.groups()) == 0:
                        # archivo encontra con string completo (sin regex)
                        logger.debug("DEBUG: CSV encontrado por clave Actividad//CalificacionesCSV completa (no regex)")
                        f_in = _
                        break
                    else:
                        # archivo encontrad con regex
                        if re_comp_match.group("Curso") == config["Curso"] and \
                            re_comp_match.group("Actividad_Nombre") == config["Actividad"]["Nombre"]:
                            logger.debug("DEBUG: CSV  encontrado por clave Actividad//CalificacionesCSV por regex (no completa)")
                            f_in = _
                            break
                        else:
                            csvRegexEncontrados.append(_)
            else:
                if len(csvRegexEncontrados) == 0:
                    raise ValueError(f"ningún archivo de {config['DirectorioEntrada']} coincide con el patrón {Actividad_calificacionesCSV_recomp.__str__()}")
                else:
                    logger.error(f"ERR: patrón buscado: {Actividad_calificacionesCSV_recomp.__str__()}:")
                    for arc in csvRegexEncontrados:
                        logger.error(f"ERR: archivo que coincide: {arc}")
                    _=f"ERR: argunos archivos de {config['DirectorioEntrada']} conciden con el patron, pero no coinciden con la Actividad y Curso pedidos ({config['Actividad']['Nombre']},{config['Curso']})"
                    logger.error(_)
                    raise ValueError(_)

            if pathlib.Path(config["DirectorioEntrada"]) is None:
                raise ValueError('config["DirectorioEntrada"] vacío, revisá "...//default')
            # if config["Actividad"]["calificacionesCSV"] is None:
            #     raise ValueError('"Actividad/calificacionesCSV" vacío, debería aplicarse "Actividad/calificacionesCSV//default"')
            f_in =  pathlib.Path(config["DirectorioEntrada"]) / f_in
            #f_in = f"Calificaciones-INF 1 15 2021-Act5 Pensamiento Algorítmico y Procedimientos-50452.csv"
            Actividad_calificacionesCSV = True
            self.CSVcalificaciones = pd.read_csv(f_in, encoding="utf8")
        except (FileNotFoundError, ValueError) as e:
            for _ in (f"revisá {args.config}: {e}",
                        f"revisá {args.config}: La clave 'CalificacionesCSV' tiene que ser el CSV  descargado de Moodle o una expresion regular.",
                        f"revisá {args.config}: Ver 'CalificacionesCSV//default' y 'CalificacionesCSV//ejemplo'. 'Curso', 'Actividad_Nombre' se reemplazan con las claves correspondientes a la actividad",
                        "Archivo de Calificaciones no encontrado."):
                logger.critical(_)
            sys.exit(1)
        except Exception:
            raise
        return self.CSVcalificaciones

    def _procesar_entrada(self,item):
        result = dict()
        result["NombreCompleto"] = item.values[1]
        result["ID"] = item.Identificador.replace("Participante", "")
        return result

    def crearDirectorios(self):
        """
        crea los directrios para  Devoluciones, listo para compactar  y subir a Moddle

        :return:
        """
        if self.CSVcalificaciones is None:
            self.CSVcalificaciones=self._cargarCSV() #todo: pasar a constuctor?
        AliasNombreDirectorio = defaultdict( lambda : None)
        dirBase=pathlib.Path(config["DirectorioSalida"]) / config["Actividad"]["Nombre"] / config["Curso"]
        logger.info(f"INFO: creando directorios en {dirBase}")
        for idx, item in self.CSVcalificaciones.iterrows():  # deueble pd.series
            estudiante = self._procesar_entrada(item)
            nuevoDir = f'{estudiante["NombreCompleto"]}_{estudiante["ID"]}_assignsubmission_file_'
            # buscl el alias para el est
            EstudianteAlias = config["EstudianteAlias"][estudiante["NombreCompleto"]]
            if EstudianteAlias is None:
                # EstudianteAlias por default,  ver clave 'EstudianteAlias//default"
                EstudianteAlias = config["EstudianteAlias//default"]["default"]
                if EstudianteAlias is None:
                    # clave no existe
                    EstudianteAlias = f"Estudiante{idx}"
                else:
                    if "{idx}" in EstudianteAlias:
                        # ok
                        EstudianteAlias = EstudianteAlias.format(idx=idx)
                    else:
                        # sin idx
                        EstudianteAlias += str(idx)
                # agrego a config, queda para salvarConfig()
                config["EstudianteAlias"][estudiante["NombreCompleto"]] = EstudianteAlias
                logger.debug(
                    f"EstudianteAlias no encontrado para '{estudiante['NombreCompleto']}': nueva clave config['EstudianteAlias'][{estudiante['NombreCompleto']}] ")
            try:
                ( dirBase / nuevoDir).mkdir(parents=True, exist_ok=True)
                AliasNombreDirectorio[EstudianteAlias]={"NombreCompleto":estudiante["NombreCompleto"],"Directorio": dirBase / nuevoDir}
            except FileNotFoundError as e:
                logger.critical(f"ERR: problema creando directorio: {e}")
            except Exception:
                raise
        logger.info(f"INFO: directorios de Devoluciones listos  para compactar  y subir a Moddle: {dirBase}")
        return AliasNombreDirectorio

    def evaluarActividad(self):
        """
        evaluar la Actividad: toma los objetivos del CSV y las entregas, y copia las entregas a la salida con estos camios:
         - inserta la devolución en el HTML
         -  anonimiza las carpetas de salida (OJO revisar que el cotenido no tenga info personal)

        Los objetivos serán las columnas a la derecha de config["CSVcolumnaFinal"]
        Ignora entregas con las columna config["CSVcolumnaCalificación"] vacia

        :return:
        """
        def _entrega_valida(dir: pathlib.Path):
            """
            busca archivos entregados

            :param dir: de la entrega
            :return: None o lista de archivos
            """
            result = None
            for _ in dir.iterdir():
                if result is None:
                    result = list()
                result.append(_)
                pass
            if result is not None and len(result) > 1:
                logger.warning(f"mas de un archivo en {dir}: {result}")
            return result

        def _busco_entregas():
            """


            :return: dict con clave Nombre y valores el FQN de las entregas
            """
            result = dict()
            NombreCompletoMoodleIDsuffix_recomp = re.compile(
                "(?P<NombreCompleto>[\w\d\ \)\(]+)_(?P<MoodleID>\d+)_(?P<Suffix>.*)")  # ,flags=re.IGNORECASE)
            directoriosRaros = list()
            directoriosVacios = list()
            # recorro las entregas y las paso por _entrega_valida()
            for dir_entrega in pathlib.Path(config["DirectorioEntrada"]).iterdir():
                _ = pathlib.Path(config["DirectorioEntrada"]) / dir_entrega
                if not _.is_dir():
                    pass
                elif _entrega_valida(_) is None:
                    directoriosVacios.append(_.name)
                else:
                    # saco NombreCompleto del dir
                    NombreCompletoMoodleIDsuffix = NombreCompletoMoodleIDsuffix_recomp.match(_.name)
                    if NombreCompletoMoodleIDsuffix is None:
                        directoriosRaros.append(_.name)
                        logger.warning(
                            f"'{_.name}' no coincide con el patroń ({NombreCompletoMoodleIDsuffix_recomp})")
                    else:
                        # si llegó acá tenemos tod_
                        entrega=_entrega_valida(_)
                        result[NombreCompletoMoodleIDsuffix.group("NombreCompleto")]=entrega[0] #ojo descarto mas de 1 archvo
            logger.debug(f"_busco_entregas(): entregas válidas procesadas: {result.values()}")
            if len(directoriosVacios)>0:
                logger.info(f"directorios vacios  ({len(directoriosVacios)}): {directoriosVacios}")
            if len(directoriosRaros)>0:
                logger.warning(
                        f"directorios raros ({len(directoriosRaros)}), no se puede extraer NombreCompleto): {directoriosRaros}")
            return result

        def _sanitizar_entrega(f_in: pathlib.Path):
            """
            lo que los pequeñxs demonixs puedan sabotear lo filtramos acá... ponele

            :param f_in:
            :return: lista de lineas con {{  aca_viene_tu_calificacion }}
            """

            #
            # sanitizar entrada
            #
            f_in_text = f_in.readlines()
            lineaBarraHTML = None
            lineaBarraBODY = None
            algoMalQueNoEstáBien = False
            for idx, linea in enumerate(f_in_text):
                if """<div class="calificaciones"> {{  aca_viene_tu_calificacion }} </div>""" in linea:  # todo: sacar hardodeado
                    break  # perdon  Niklaus
                elif """</html>""" in linea or """</html>""" in linea:
                    lineaBarraHTML = idx
            else:
                algoMalQueNoEstáBien = True  # todo ok
            if algoMalQueNoEstáBien:
                logger.warning(f"algoMalQueNoEstáBien en {f_in}")
                if lineaBarraHTML is None:
                    lineaBarraHTML = len(f_in_text) - 1
                f_in_text.insert(lineaBarraHTML,
                                 """<div class="calificaciones"> {{  aca_viene_tu_calificacion }} </div>""")  # todo: sacar hardodeado)
            return f_in_text

        aliasNombreDirectorio = self.crearDirectorios() #con Alias obtengo nombre y Directorio
        nombreDirectorio=defaultdict( lambda : None) #indexo, con Nombre obntego Alias y Directorio
        for _ in aliasNombreDirectorio.keys():
            nombreDirectorio[aliasNombreDirectorio[_]["NombreCompleto"]]=aliasNombreDirectorio[_]

        nombreEntrega=_busco_entregas() #con el Nombre obtengo el FQN de la entrega

        for nombre in nombreEntrega.keys():
            if nombre not in nombreDirectorio:
                logger.warning(f"entrega ignorada, '{nombre}': no está en el directorio de salida (bug?)")
            else:
                try:
                    with open(str(nombreEntrega[nombre]),mode="r") as f_in:
                        f_in_text=_sanitizar_entrega(f_in)
                        template = j2.Environment( loader=j2.BaseLoader ).from_string("".join(f_in_text))
                        f_out=pathlib.Path(nombreDirectorio[nombre]["Directorio"])
                        if nombreEntrega[nombre].name.upper().endswith("HTML"):
                            f_out = f_out /  nombreEntrega[nombre].name
                        else:
                            f_out = f_out / ( nombreEntrega[nombre].name+".html")
                        with open(str(f_out),"w")  as f_out:
                            f_out.write(template.render(aca_viene_tu_calificacion=self.notas(nombre)))
                except Exception as e:
                    logger.warning(f" problema al crear salida de '{nombre}': {e}")

    def notas(self, nombre: str, HTML=False):
        """
        devuelve las notas

        :param nombre:
        :return:
        """
        def _CSVcolumnaFinal():
            """
            busca el nro de columna final
            :return:
            """
            result = config["Moodle"]["CSVcolumnaFinal"] #todo: hacer busqueda
            try:
                result=int(result)
            except Exception:
                raise ValueError("config['Moodle']['CSVcolumnaFinal']")
            return result

        result="<p>califaciones no encontradas :(</p>"
        if HTML:
            raise NotImplementedError("de debo la salida formatead en HTML, MUY VERDE")
        for index, calificacion in self.CSVcalificaciones.iterrows(): #todo: al pedo usamos pandas... un poco de dignidad por favor
            if calificacion[1] == nombre:
                result=[
"""<style type="text/css">
.tg  {border-collapse:collapse;border-color:#aaa;border-spacing:0;}
.tg td{background-color:#fff;border-color:#aaa;border-style:solid;border-width:1px;color:#333;
  font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{background-color:#f38630;border-color:#aaa;border-style:solid;border-width:1px;color:#fff;
  font-family:Arial, sans-serif;font-size:14px;font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>""","""<table class="tg">""",]
                try:
                    result.append(
                        f"""<tr><th class="tg-0lax">Calificacion</th><th class="tg-0lax">{calificacion["Calificación"]}</th></tr>""")
                except Exception:
                    pass
                resultObjetivos=calificacion.keys()[_CSVcolumnaFinal():]
                resultNotas=calificacion[resultObjetivos]
                for obj in resultObjetivos:
                    result.append(f"""<tr><th class="tg-0lax">{obj}</th><th class="tg-0lax">{calificacion[obj]}</th></tr>""")
                else:
                    result.append("<table>")
                break #perdon Niklaus
        else:
            logger.warning(f"notas no encontradas para {nombre}")
        return "".join(result)

def cargarConfig():
    global config
    #print(cargarArgumentosSegunPasos)
    def sale_defaultdict(*args,**kwargs):
        result=defaultdict(lambda: None)
        result.update(args[0])
        return result
    with open(args.config) as fh:
        logger.warning(f"Cargando config de '{args.config}'")
        try:
            config=json.load(fh,object_hook=sale_defaultdict)
        except json.JSONDecodeError as e:
            logger.critical(f"Problema de formato en el archivo de configuración ({fh}): {e}")
            sys.exit(1)
        except FileNotFoundError as e:
            logger.critical(f"No se encuentra el archivo de configuración: usá el parámetro -c")
            sys.exit(1)
        except:
            raise
    #
    # cargo defaults para config y  config["Actividad"]
    #
    for config_nivel in (config["Actividad"],config):
        config_nuevos=dict()
        for config_key in config_nivel.keys():
            if config_key.endswith("//default"):
                config_key_0=config_key.split("//")[0]
                if config_key_0 not in config_nivel.keys():
                    config_nuevos[config_key_0] = config_nivel[config_key]
        #for _ in ["Entrada", "Salida"]:
        if config_nivel["DirectorioEntrada" ] is None:
            config_nuevos["DirectorioEntrada" ] = str(pathlib.Path.cwd())
        if config_nivel["DirectorioSalida" ] is None:
            config_nuevos["DirectorioSalida" ] = str(pathlib.Path.cwd().home())
        config_nivel.update(config_nuevos)
    #pprint(config)

def guardarConfig():
    global config
    config_nuevo=args.config.replace(".json","-nuevo.json")
    with open(config_nuevo,mode="w") as fh:
        logger.info(f"Guardando config en '{config_nuevo}'")
        json.dump(config,fh)

def cargarArgumentosParaActividad(actividad:ActividadConPasos):
    global args
    parserMain = argparse.ArgumentParser(description='Actividad HTML con Moodle',
                                         usage= """\r
                                                   \rtarmoodle.py -c \"config.json\" --pase \"evaluarActividad\"""",
                                         add_help=True)
    parserMain.add_argument('-c', '--config', action="store", dest="config", nargs='+', help="archivo de configuracion JSON", type=str, required=False, default="config.json")
    parserMain.add_argument('-p', '--paso', action="store", dest="paso", help="crearActividad: necesita ", type=str, required=True)
    #parserMain.add_argument('-o', action="store", dest="output_file", help="Output file (.xlsx)", type=str, required=True)
    #parserMain.add_argument('-l', action="store", dest="loco_id", nargs='+', help="Locomotive number", type=int, required=False)
    args=parserMain.parse_args()
    try:
        listaPasos = [paso for paso in dir(ActividadConPasos) if not paso.startswith('_')]
        if args.paso in listaPasos:
            args.paso = getattr(actividad,args.paso)
        else:
            args.paso=int(args.paso)
            raise NotImplementedError(f"paso por nro ({args.paso}) todavía no implementado, usar {listaPasos}")#, solo existen del 0 al {len(ActividadConPasos.keys()) - 1}: {tuple(ActividadConPasos.keys())}")
            #(f"ActividadConPasos por nro ({args.paso}) no implementdo")# essiste, solo existen del 0 al {len(ActividadConPasos.keys())-1}: {tuple(ActividadConPasos.keys())}")
            # if args.paso >  len(ActividadConPasos.keys())-1 or args.paso < 0:
            #
            # else:
            #     args.paso=list(ActividadConPasos.keys())[args.paso]
    except ValueError as e:
            print(f"paso '{args.paso}' no essiste, solo existen: {listaPasos}")
            exit(-1)
    except NotImplementedError as e:
        print(e)
        exit(-1)
    except Exception as e:
        raise #nada que hacer...
    return args


if __name__ == '__main__':
    pass