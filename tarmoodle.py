"""
Utilidad para crear actividades con Moodle



(c)2020 Leandro Batlle, ADICRA - GPLv3
"""
import logging

import tarmoodle
from tarmoodle import *


if __name__=="__main__":
    #
    # logger a consola
    #
    logger = tarmoodle.logger
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    #
    # prepara entorno (config
    #
    miAct = ActividadConPasos()
    args = cargarArgumentosParaActividad(miAct)
    cargarConfig()
    logger.info(f"cargando entregas y califiaciones de {tarmoodle.config['DirectorioEntrada']}")
    logger.info(f"sacando devoluciones y califiaciones en {tarmoodle.config['DirectorioSalida']}")
    #
    # ejecutar paso pedido
    #
    args.paso()
    guardarConfig()