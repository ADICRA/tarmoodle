# tarmoode - Utilidades para el módulo Tareas de Moodle

tarmoodle ayuda a usar el módulo Tareas de Moodle. Procesa el CSV "Hoja de Calificaciones" y crea carpetas para reparto masivo de archivos.


## instalación

```bash
cd tarmoodle
poetry install
```

ayuda:

```bash
poetry shell
python tarmoodle.py  
```


## pasos de las actividades

### Proceso de Calificaciones.csv

```bash
poetry shell
python tarmoodle.py  --paso evaluarActividad
```


### Creación de devoluciones masivas

```bash
poetry shell
python tarmoodle.py  --paso crearDirectorios
```



## archivo de configuración

Editar el archivo config.json. Las claves de la actividad son: 

### Nivel 0

- Curso

### Nivel 1: Actividad

- Nombre
- CalificacionesCSV

